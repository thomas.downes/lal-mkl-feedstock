# lal-mkl-feedstock

1. conda create -n mkl_builder
1. conda activate mkl_builder
1. conda config --env --append channels conda-forge/label/gcc7
1. conda config --env --append channels conda-forge
1. conda install conda-build conda-verify python=3.6
1. tar xvf lal-6.19.0.1.tar.xz
   * this was produced from master first week of December after a few Intel FFT-related patches
1. conda build --numpy 1.15 meta.yaml
   * on macOS you must follow [these instructions](https://wiki.ligo.org/Computing/CondaPackaging#A_40macOS_41_Build_tries_to_use_the_wrong_version_of_the_MacOS_SDK)
to install the macOS 10.9 Software Development Kit